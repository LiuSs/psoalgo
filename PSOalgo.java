import java.util.*;
import java.io.*;

class PSOalgo{
	public static void main(String[] args){
		//自訂題目：求解y = x + x^2 + x^4 之最小值
		//經測試，x = -0.39 時，y=-0.21476559 ; x= -0.38 y=-0.2147
		//該方程式只有一個輸入，為一維PSO

		//*****************定義初始參數(設定十個隨機點)*****************
		double[] position = new double[10];//x位置
		double[] velocity = new double[10];//x速度
		double[] fitness = new double[10];//x適應值
		double[] Pbest = new double[10];//每個x的局部最佳位置(此值若比position還更好，則將此值直接取代position)
		double[] Pfitness = new double[10];//x的局部適應值(此值若比fitness還更好，則將此值直接取代fitness)

		int i=0;//迴圈變數，同時代表粒子的編號
		double c1=1.0,c2=1.0;//學習因子(1.0 -> 2.5)
		int t=1;//當前迭代次數
		double w=0.9;//慣性權重(0.9 -> 0.4)
		double rand = Math.random();//設定隨機變數

		for(i=0;i<position.length;i++){
			position[i] = Math.random()*(-100) + Math.random()*100;//設定初始化位置介於-100~100
			Pbest[i] = position[i];//初始時將每個x的局部解設定成和當前一致(因為尚無上一個位置可比較)
		}

		for(i=0;i<fitness.length;i++){
			fitness[i] = position[i] + position[i]*position[i] + position[i]*position[i]*position[i]*position[i]; //設定初始適應值，即算出x所對應的y值
			Pfitness[i] = fitness[i];//初始時將每個x的局部適應值設定成和當前一致
		}

		for(i=0;i<velocity.length;i++)
			velocity[i] = Math.random()*(-4) + Math.random()*4;//設定初始化速度介於-4~4

		double Gfitness  = Pfitness[0];//找出初始的全域最佳解
		double Gbest = Pbest[0];
		for(i=1;i<Pfitness.length;i++){
			if(Pfitness[i]<Gfitness){
				Gfitness = Pfitness[i];
				Gbest = Pbest[i];
			}
		}

		for(i=0;i<position.length;i++)
			System.out.printf("x"+i+"的位置目前在 %.6f，其速度和適應值分別是：%.6f  %.6f\n",position[i],velocity[i],fitness[i]);
		System.out.println();

		System.out.printf("當前最佳位置目前在 %.6f，其適應值是：%.6f\n",Gbest,Gfitness);


		//*****************初始化完成*****************

		Scanner br = new Scanner(System.in);//根據使用者的輸入取得迭代次數
		int count = br.nextInt();

		//*****************PSO開始*****************
		while(t<count){
		//*****************更新每個粒子的速度*****************
			for(i=0;i<velocity.length;i++){
				velocity[i] = w*velocity[i] + c1*rand*(Pbest[i]-position[i]) + c2*rand*(Gbest-position[i]);

			if(velocity[i]>4 || velocity[i]<(-4))//若速度超過或低於，給予新的速度
				velocity[i] = Math.random()*(-4) + Math.random()*4;
		}

		//*****************更新每個粒子的位置，同時檢測是否為局部最佳解*****************
			for(i=0;i<position.length;i++){
				position[i] = position[i] + velocity[i];

				if(position[i]>100 || position[i]<(-100))//如果粒子位置超過範圍，則隨機再給予一次新位置
					position[i] = Math.random()*(-100) + Math.random()*100;

				fitness[i] = position[i] + position[i]*position[i] + position[i]*position[i]*position[i]*position[i];//利用新的位置來計算新的適應值
				if(fitness[i]<Pfitness[i]){ //因為題目是求解最小值，故適應值當然是愈小愈好(直接和舊值比較)
					Pfitness[i] = fitness[i];//除了更新局部位置，適應值也要換新
					Pbest[i] = position[i];
				}
			}

		//*****************更新全域最佳解*****************
			for(i=0;i<Pfitness.length;i++){
				if(Pfitness[i]<Gfitness){
					Gfitness = Pfitness[i];
					Gbest = Pbest[i];
				}
			}

		//*****************更新慣性權重以及學習因子(參考其他文獻)*****************
		w = w - (w-0.4)/count * t;//文獻上指稱，權重的線性遞減由 0.9 -> 0.4 較為適合
		c1 = c1 + (2.5-c1)/count;//學習因子的成長曲線
		c2 = c2 +(2.5-c2)/count;

		//*****************顯示當前結果*****************
		for(i=0;i<velocity.length;i++)
			System.out.printf("x"+i+"的位置目前在 %.6f，其速度和適應值分別是：%.6f  %.6f\n",position[i],velocity[i],fitness[i]);
		System.out.println();

		t++;//更新迭代次數
	   }
	   //*****************PSO結束*****************

	  System.out.printf("最佳位置為 %.6f ，適配值為 %.6f\n",Gbest,Gfitness);

	}
}